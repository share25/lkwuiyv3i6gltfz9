package com.nununu.currencylistexample.reusable

import androidx.compose.foundation.layout.*
import androidx.compose.material.Icon
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.semantics.SemanticsPropertyKey
import androidx.compose.ui.semantics.SemanticsPropertyReceiver
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp
import com.nununu.currencylistexample.R
import com.nununu.currencylistexample.models.MessageType
import com.nununu.currencylistexample.models.Message


/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

val DrawableId = SemanticsPropertyKey<Int>("drawableResId")
var SemanticsPropertyReceiver.drawableId by DrawableId


/**
 * Present a message in UI. It shows content of the message with an icon next to it.
 * Depending on [MessageType] different icon is shown.
 * @param [message] Message to be presented
 */
@Composable
fun FullScreenMessage(message: Message) {


    val iconResource = when (message.type) {
        MessageType.Error -> {
            R.drawable.ic_error
        }
        MessageType.Loading -> {
            R.drawable.ic_update
        }
    }
    Column(verticalArrangement = Arrangement.Center) {
        Row(modifier = Modifier
            .fillMaxWidth()
            .heightIn(min = 60.dp)
            .padding(end = 5.dp),
            horizontalArrangement = Arrangement.Center
        ) {
            Icon(
                painter = painterResource(id = iconResource),
                contentDescription = "Message Icon",
                modifier = Modifier.padding(5.dp).semantics { drawableId = iconResource }
            )
            Text(modifier = Modifier
                .align(alignment = Alignment.CenterVertically)
                .testTag("MessageText"),

                text = message.text)
        }
    }
}