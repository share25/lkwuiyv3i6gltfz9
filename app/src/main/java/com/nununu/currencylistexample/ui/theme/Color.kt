package com.nununu.currencylistexample.ui.theme

import androidx.compose.ui.graphics.Color


val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val MidGrey = Color(0.5f,0.5f,0.5f,1f)
val MidLightGrey = Color(0.8f,0.8f,0.8f,1f)
val LightGrey = Color(0.9f,0.9f,0.9f,1f)
val DarkGrey = Color(0.2f,0.2f,0.2f,1f)
val MidDarkGrey = Color(0.3f,0.3f,0.3f,1f)