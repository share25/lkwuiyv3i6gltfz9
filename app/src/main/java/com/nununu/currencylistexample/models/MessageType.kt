package com.nununu.currencylistexample.models


/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */


enum class MessageType() {
    Error,
    Loading
}