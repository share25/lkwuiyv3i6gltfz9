package com.nununu.data

import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.Test

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

class DataRepositoryTest {

    private val dataService : DataService = mockk()
    private val dataRepository = DataRepository(dataService)

    @Test
    fun `when getAllCurrencies is called then dataService gets call`()= runTest {
        coEvery { dataService.getAllCurrencies() } returns Result.failure(Exception())
        dataRepository.getAllCurrencies()

        coVerify { dataService.getAllCurrencies() }
    }


}