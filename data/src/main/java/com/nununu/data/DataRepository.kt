package com.nununu.data

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

/**
 * Repository to manage queries to data service
 */
class DataRepository(
    private val dataService: DataService
) {
    /**
     * Get a list of all currencies from database.
     * @return Result.Success<List<CurrencyInfo>> in case of a successful call
     * @return Result.Failure<Throwable> in case of a failed call
     */
    suspend fun getAllCurrencies() = dataService.getAllCurrencies()
}