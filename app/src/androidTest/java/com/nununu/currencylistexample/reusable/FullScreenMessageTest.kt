package com.nununu.currencylistexample.reusable

import androidx.annotation.DrawableRes
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.junit4.createComposeRule
import com.nununu.currencylistexample.MainActivity
import com.nununu.currencylistexample.R
import com.nununu.currencylistexample.models.Message
import com.nununu.currencylistexample.models.MessageType
import com.nununu.currencylistexample.ui.theme.CurrencyListExampleTheme
import org.junit.Rule
import org.junit.Test

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

class FullScreenMessageTest {

    @get:Rule
    val composeTestRule = createComposeRule()

    private fun hasDrawable(@DrawableRes id: Int): SemanticsMatcher =
        SemanticsMatcher.expectValue(DrawableId, id)

    @Test
    fun test_display_of_error_message() {
        val message = Message(MessageType.Error,"Lorem Ipsum")
        composeTestRule.setContent {
            CurrencyListExampleTheme {
                FullScreenMessage(message = message)
            }
        }
        composeTestRule.onNodeWithTag("MessageText").assert(hasText(message.text))
        composeTestRule.onNode(hasDrawable(R.drawable.ic_error)).assertIsDisplayed()
    }

    @Test
    fun test_display_of_loading_message() {
        val message = Message(MessageType.Loading,"Lorem Ipsum Dolores")
        composeTestRule.setContent {
            CurrencyListExampleTheme {
                FullScreenMessage(message = message)
            }
        }
        composeTestRule.onNodeWithTag("MessageText").assert(hasText(message.text))
        composeTestRule.onNode(hasDrawable(R.drawable.ic_update)).assertIsDisplayed()
    }
}