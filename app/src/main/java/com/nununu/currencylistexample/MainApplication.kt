package com.nununu.currencylistexample

import android.app.Application
import com.nununu.data.dataModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

class MainApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin{
            androidContext(this@MainApplication)
            modules(appModule, dataModule)
        }
    }
}