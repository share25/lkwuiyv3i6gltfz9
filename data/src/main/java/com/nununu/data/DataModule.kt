package com.nununu.data

import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

val dataModule = module {

    single{
        DataRepository(
            dataService = get()
        )
    }

    single<DataService> {
        JsonFileDataService(androidContext().resources.openRawResource(R.raw.currency_list))
    }
}