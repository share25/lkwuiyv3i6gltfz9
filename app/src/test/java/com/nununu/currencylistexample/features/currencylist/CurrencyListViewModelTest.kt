package com.nununu.currencylistexample.features.currencylist

import android.util.Log
import com.nununu.data.CurrencyInfo
import com.nununu.data.DataRepository
import io.mockk.coEvery
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

class CurrencyListViewModelTest {

    private val testDispatcher = UnconfinedTestDispatcher()

    private val dataRepository : DataRepository = mockk()
    private val viewModel = CurrencyListViewModel(dataRepository)

    private val currencyList = listOf(
            CurrencyInfo("ETH","Ethereum","ETH"),
            CurrencyInfo("BTC","Bitcoin", "BTC"))

    private val currencyListSorted = listOf(
        CurrencyInfo("BTC","Bitcoin", "BTC"),
        CurrencyInfo("ETH","Ethereum","ETH")
    )

    @Before
    fun startup() {
        Dispatchers.setMain(testDispatcher)
    }
    init {
        mockkStatic(Log::class)
        every { Log.i(any(), any()) } returns 0
    }


    @After
    fun teardown() {
        Dispatchers.resetMain()
    }

    @Test
    fun `when the view model inits then uistate goes to list not loaded`() {
        assertEquals(CurrencyListState.ListNotLoaded,viewModel.uiState)
    }

    @Test
    fun `when getAllCurrencies is successful then ListLoaded state is set`() = runTest() {
        coEvery { dataRepository.getAllCurrencies() } returns Result.success(currencyList)

        viewModel.getAllCurrencies()

        assertEquals(CurrencyListState.ListLoaded(currencyList),viewModel.uiState)
    }

    @Test
    fun `when getAllCurrencies fails then UnexpectedState state is set`() = runTest {
        coEvery { dataRepository.getAllCurrencies() } returns Result.failure(Exception())

        viewModel.getAllCurrencies()

        assertEquals(
            CurrencyListState.UnexpectedState,
            viewModel.uiState)
    }

    @Test
    fun `given list loaded when sort list is triggered then list is sorted alphabetically and uiState updated`() = runTest {
        coEvery { dataRepository.getAllCurrencies() } returns Result.success(currencyList)
        //given
        viewModel.getAllCurrencies()
        //when
        viewModel.sortList()
        //then
        assertEquals(CurrencyListState.ListLoaded(currencyListSorted),viewModel.uiState)
    }
}