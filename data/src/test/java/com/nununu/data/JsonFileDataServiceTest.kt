package com.nununu.data

import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.runTest
import org.junit.Assert.*
import org.junit.Test
import java.io.InputStream

class JsonFileDataServiceTest() {

    val validString = "[{\n" +
            "        \"id\": \"BTC\",\n" +
            "        \"name\": \"Bitcoin\",\n" +
            "        \"symbol\": \"BTC\"\n" +
            "    }]"

    val invalidString = "[{\n" +
            "        \"id11\": \"BTC\",\n" +
            "        \"name\": \"Bitcoin\",\n" +
            "        \"symbol\": \"BTC\"\n" +
            "    }]"

    @Test
    fun `given valid input stream returns list of CurrencyInfo successfully`() = runTest {
        val expectedOutput = listOf(CurrencyInfo("BTC","Bitcoin","BTC"))

        val jsonFileDataService = JsonFileDataService(validString.byteInputStream())

        val currenciesResult = jsonFileDataService.getAllCurrencies()

        assertTrue(currenciesResult.isSuccess)
        assertEquals(expectedOutput,currenciesResult.getOrNull())
    }

    @Test
    fun `given invalid input stream returns failure`() = runTest {

        val jsonFileDataService = JsonFileDataService(invalidString.byteInputStream())

        val currenciesResult = jsonFileDataService.getAllCurrencies()

        assertTrue(currenciesResult.isFailure)
        assertNotNull(currenciesResult.exceptionOrNull())
    }
}