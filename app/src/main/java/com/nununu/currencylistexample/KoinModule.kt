package com.nununu.currencylistexample

import com.nununu.currencylistexample.features.currencylist.CurrencyListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel


import org.koin.dsl.module

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

val appModule = module {

    viewModel {
        CurrencyListViewModel(
            dataRepo = get()
        )
    }
}