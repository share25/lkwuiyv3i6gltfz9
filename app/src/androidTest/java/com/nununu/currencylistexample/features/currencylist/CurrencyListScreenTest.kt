package com.nununu.currencylistexample.features.currencylist

import androidx.compose.ui.test.assert
import androidx.compose.ui.test.assertIsDisplayed
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.performClick
import com.nununu.currencylistexample.MainActivity
import com.nununu.currencylistexample.reusable.FullScreenMessage
import com.nununu.currencylistexample.ui.theme.CurrencyListExampleTheme
import org.junit.Assert.*
import org.junit.Rule
import org.junit.Test

class CurrencyListScreenTest {

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    @Test
    fun load_list_button_click_shows_list() {
        composeTestRule.setContent {
            CurrencyListExampleTheme {
                ListScreen()
            }
        }
        composeTestRule.onNodeWithTag("loadListButton").performClick()
        composeTestRule.onNodeWithTag("sortButton").assertIsDisplayed()
        composeTestRule.onNodeWithTag("currencyListColumn").assertIsDisplayed()
    }
}