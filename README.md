# LkwUiYv3i6GLTFz9

## Introduction
Android application presenting jetpack compose lazy column to show a list of crypto currencies.


## App developer insight
### Structure
The app is divided into two modules:
- main one, containing all UI related implementation
- data, serving purpose of a typical data layer in app architecture

Data module is a dependency of the main module

#### Main module
UI layer has been developed based on a single activity with one main composable screen `CurrencyListScreen`. Content of the screen is controlled by the only viewModel, `CurrencyListViewModel`. 

The main flow of the app consists of:
- loading list of currencies 
- presenting list
- optionaly sorting the list by using a button located in top-rgith part of the app
- the app handles events of an item in the list clicked, but it logs only an INFO message to the log

##### Current limitations:
- currently app is locked to use vertical orientation only
- worth adding compose navigation and build fundations for potential application grow. 

#### Data module
Data module defines interface for data service (`DataService`) to fit different types of managing data (e.g. cached data, remote db or read access only). The module implements the interface with `JsonFileDataService` to be able to load information from a locally stored json file. The file is stored in app's assets so only read access is available.  

`DataService` interface together with an extra layer (`DataRepository`) allows simply replacing `JsonFileDataService` with a more sophisticated solution in case of evolution or expanse of requirements (fetching requirements from a cloud, adding some live data to currencies) 

#### Dependency injection framework

The app uses Koin as dependency injection framework. There are two Koin modules, one per app module. 

#### Other

1. To make sure all app modules use the same versions of libraries they are listed in a separate file `dependencies.gradle`
2. The project has a CI pipeline setup, but only non UI tests are chosen to run. 