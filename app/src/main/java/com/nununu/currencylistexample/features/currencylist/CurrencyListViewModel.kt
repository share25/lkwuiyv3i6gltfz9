package com.nununu.currencylistexample.features.currencylist

import android.os.Parcelable
import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.nununu.data.CurrencyInfo
import com.nununu.data.DataRepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

sealed class CurrencyListState {
    object ListNotLoaded : CurrencyListState()
    data class ListLoaded(val list: List<CurrencyInfo>) : CurrencyListState()
    object UnexpectedState : CurrencyListState()
    object ListLoading : CurrencyListState()
}

class CurrencyListViewModel(
    private val dataRepo : DataRepository,
) : ViewModel() {
    private val TAG = "CurrencyListViewModel"
    var uiState by mutableStateOf<CurrencyListState>(CurrencyListState.ListNotLoaded)
        private set

    private var sortJob : Job? = null


    /**
     * Get list of all currencies from data repository.
     * @return [CurrencyListState.ListLoaded] with list of currency if database query was successful.
     * @return [CurrencyListState.UnexpectedState] In case of query failure returns
     */
    fun getAllCurrencies() {
        viewModelScope.launch {
            uiState = CurrencyListState.ListLoading
            dataRepo.getAllCurrencies().fold(
                onSuccess = {
                    uiState = CurrencyListState.ListLoaded(it)
                },
                onFailure = {
                    Log.i(TAG,it.message?: "Error occurred while querying db")
                    uiState = CurrencyListState.UnexpectedState
                }
            )
        }
    }

    /**
     * Sort list of currencies by currency's name in alphabetical order
     */
    fun sortList() {
        if (sortJob?.isActive != true) {
            sortJob = viewModelScope.launch {
                uiState.let { currentState ->
                    if (currentState is CurrencyListState.ListLoaded) {

                        uiState = CurrencyListState.ListLoaded(currentState.list.sortedBy { it.name })
                        Log.i(TAG,"Sorted")
                    }
                }
            }
        } else {
            Log.i(TAG,"Sorting is happening")
        }
    }

    /**
     * Log what item was clicked
     * @param [symbol] symbol of currency item clicked
     */
    fun logItemClick(symbol : String) {
        Log.i(TAG,"$symbol has been tapped and callback has been handled.")
    }
}