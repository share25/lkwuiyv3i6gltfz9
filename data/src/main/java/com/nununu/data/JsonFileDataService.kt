package com.nununu.data

import android.content.res.AssetManager
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.json.Json.Default.decodeFromString
import java.io.InputStream


/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

class JsonFileDataService(
    private val dbFileInputStream: InputStream
) : DataService {
    /**
     * Cached list of currencies
     */
    private var cachedCurrencyInfo : List<CurrencyInfo>? = null

    /**
     * Get a list of all currencies from [dbFileInputStream] used to initialise [JsonFileDataService]
     * or from within class cached version. When using [dbFileInputStream] it is expected to receive
     * a json file containing list of items deserialisable to [CurrencyInfo]
     * @return Result.Success<List<CurrencyInfo>> in case of a successful call
     * @return Result.Failure<Throwable> in case of a failed call
     */
    override suspend fun getAllCurrencies(): Result<List<CurrencyInfo>> = runCatching {
        cachedCurrencyInfo?.let {
            it
        } ?: run {
            dbFileInputStream.bufferedReader()
                .use{it.readText()}
                .let{
                    val currencyList = decodeFromString(ListSerializer(CurrencyInfo.serializer()),it)
                    cachedCurrencyInfo = currencyList
                    currencyList
                }
        }
    }
}