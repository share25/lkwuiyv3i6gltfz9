package com.nununu.currencylistexample.features.currencylist

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.nununu.currencylistexample.R
import com.nununu.currencylistexample.cryptoSymbolResourceResolver
import com.nununu.currencylistexample.models.Message
import com.nununu.currencylistexample.models.MessageType
import com.nununu.currencylistexample.reusable.FullScreenMessage
import com.nununu.data.CurrencyInfo
import com.nununu.data.Symbol
import org.koin.androidx.compose.get

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */


/**
 * Main composable directly connected to [CurrencyListViewModel] to react to uiState changes.
 */
@Composable
fun ListScreen(viewModel : CurrencyListViewModel = get()){

    when(val uiState = viewModel.uiState) {
        is CurrencyListState.ListNotLoaded -> {
            UnloadedList { viewModel.getAllCurrencies() }
        }
        is CurrencyListState.ListLoading -> {
            LoadingList()
        }
        is CurrencyListState.ListLoaded -> {
            LoadedList(uiState.list,
            onSortButtonClick = {viewModel.sortList()},
                onItemClick = {symbol -> viewModel.logItemClick(symbol)})
        }
        is CurrencyListState.UnexpectedState -> {
            ErrorListLoading(message = stringResource(id = R.string.issue_bringing_list))
        }

    }
}
/**
 * Present [FullScreenMessage] with a user friendly message to notify them about a caught error
 */
@Preview
@Composable
fun ErrorListLoading(
    @PreviewParameter(FakeErrorMessageProvider::class) message:String) {
    FullScreenMessage(Message(MessageType.Error,message))
}

/**
 * Full screen presenting list of currencies. It has two sections divided by a thin divider.
 * Top section contains button to sort [theList]
 * Main section containing [LazyColumn] of currencies provided in [theList]
 */
@Composable
fun LoadedList(theList : List<CurrencyInfo>,
               onSortButtonClick: () -> Unit,
                onItemClick: (Symbol) -> Unit) {
    Column() {
        Row(
            Modifier
                .align(Alignment.End)
                .padding(
                    horizontal = dimensionResource(id = R.dimen.large_padding),
                    vertical = dimensionResource(id = R.dimen.small_padding)
                )
        ) {
            OutlinedButton(
                onClick = onSortButtonClick,
                modifier = Modifier.testTag("sortButton")) {
                Text(stringResource(id = R.string.sort_alphabetically))
            }
        }
        Divider(
            color = MaterialTheme.colors.surface,
            thickness = dimensionResource(id = R.dimen.tiny_padding)
        )
        Row(Modifier.fillMaxHeight()) {
            LazyColumn(Modifier
                .weight(1f)
                .testTag("currencyListColumn")) {
                theList.forEach {
                    item{
                        CurrencyListItem(item = it) {onItemClick(it.symbol)}
                    }
                }
            }
        }
    }
}

/**
 * Show in one row:
 * - logo of the currency or first letter of currency's symbol if logo not available
 * - name of the currency
 * - symbol of the currency
 * - chevron to give a hint to the user that the item is clickable
 * Below the row with content there is a small full width divider
 */
@Preview
@Composable
fun CurrencyListItem(
    @PreviewParameter(FakeCurrencyListItemProvider::class) item : CurrencyInfo,
    onItemClick: () -> Unit = {}) {
    Row(modifier = Modifier
        .heightIn(32.dp)
        .fillMaxWidth()
        .padding(
            start = dimensionResource(id = R.dimen.small_padding),
            end = dimensionResource(id = R.dimen.small_padding),
            top = dimensionResource(id = R.dimen.medium_padding),
            bottom = 3.dp
        )
        .clickable {
            onItemClick()
        }) {
        item.symbol.cryptoSymbolResourceResolver()?.also{ drawableRes ->
            Icon(
                painter = painterResource(id = drawableRes),
                contentDescription = null,
                tint= Color.Unspecified,
                modifier = Modifier
                    .padding(dimensionResource(id = R.dimen.medium_padding))
                    .size(32.dp)
            )
        } ?: also {
            Text(item.name[0].toString(),
                modifier = Modifier
                    .align(Alignment.CenterVertically)
                    .padding(horizontal = dimensionResource(id = R.dimen.medium_padding))
                    .size(32.dp)
                    .background(MaterialTheme.colors.primary, shape = CircleShape),
                textAlign = TextAlign.Center,
                fontSize = 24.sp,
                color = Color.White
            )
        }
        Text(item.name,
            modifier = Modifier
                .weight(1f)
                .align(Alignment.CenterVertically),
            textAlign = TextAlign.Start)
        Text(item.symbol,
            modifier = Modifier
                .align(Alignment.CenterVertically)
                .padding(end = dimensionResource(id = R.dimen.large_padding)),
            textAlign = TextAlign.End)
        Icon(painter = painterResource(id = R.drawable.ic_chevron), contentDescription = null)
    }
    Divider(
        color = MaterialTheme.colors.surface,
        thickness = dimensionResource(id = R.dimen.small_padding)
    )
}

/**
 * Shows button to load list of currencies
 * @param [buttonCallback] Callback to take on the only button click
 */
@Preview
@Composable
fun UnloadedList(buttonCallback: () -> Unit = {}) {
    Box() {
        OutlinedButton(
            modifier = Modifier
                .size(128.dp, 48.dp)
                .align(Alignment.Center)
                .testTag("loadListButton"),
            onClick = buttonCallback) {
            Text(stringResource(id = R.string.load_list))
        }
    }
}

/**
 * Present [FullScreenMessage] while list is being loaded asynchronously
 */
@Preview
@Composable
fun LoadingList() {
    FullScreenMessage(
        Message(MessageType.Loading,
            stringResource(id = R.string.list_is_being_prepared)))
}


class FakeErrorMessageProvider : PreviewParameterProvider<String> {
    override val values: Sequence<String>
        get() = sequenceOf("Lorem Ipsum")
}

class FakeCurrencyListItemProvider : PreviewParameterProvider<CurrencyInfo> {
    override val values: Sequence<CurrencyInfo>
        get() = sequenceOf(CurrencyInfo(
            id = "BTC",
            name = "Bitcoin",
            symbol = "BTC"
        ))
}