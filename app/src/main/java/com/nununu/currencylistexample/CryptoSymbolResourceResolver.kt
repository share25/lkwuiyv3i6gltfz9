package com.nununu.currencylistexample

import com.nununu.data.Symbol

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

/**
 * Based on symbol of a currency refer to appropriate drawable resource to provide its logo
 */
fun Symbol.cryptoSymbolResourceResolver() : Int? {
    return when(this) {
        "BCH" -> R.drawable.ic_bch_symbol
        "BNB" -> R.drawable.ic_bnb_symbol
        "BTC" -> R.drawable.ic_btc_symbol
        "CRO" -> R.drawable.ic_cro_symbol
        "EOS" -> R.drawable.ic_eos_symbol
        "ETC" -> R.drawable.ic_etc_symbol
        "ETH" -> R.drawable.ic_eth_symbol
        "LINK" -> R.drawable.ic_link_symbol
        "LTC" -> R.drawable.ic_ltc_symbol
        "NEO" -> R.drawable.ic_neo_symbol
        "ONT" -> R.drawable.ic_ont_symbol
        "USDC" -> R.drawable.ic_usdc_symbol
        "XRP" -> R.drawable.ic_xrp_symbol
        else -> null
    }
}