package com.nununu.currencylistexample.models


/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

data class Message(
    val type: MessageType,
    val text: String
)
