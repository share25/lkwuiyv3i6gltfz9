package com.nununu.data

import kotlinx.serialization.Serializable

/**
 * CurrencyListExample
 *
 * Copyright (C) 2022, Kacper Bierzanowski - All Rights Reserved.
 */

@Serializable
data class CurrencyInfo(
    val id : String,
    val name : String,
    val symbol : Symbol
)

typealias Symbol = String